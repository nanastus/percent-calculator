import Calc from './calculator.mjs'

const DOM = {
  /** @type {HTMLInputElement} */
  FIRST1: document.getElementById('first1'),
  /** @type {HTMLInputElement} */
  SECOND1: document.getElementById('second1'),
  /** @type {HTMLInputElement} */
  RESULT1: document.getElementById('result1'),
  /** @type {HTMLInputElement} */
  FIRST2: document.getElementById('first2'),
  /** @type {HTMLInputElement} */
  SECOND2: document.getElementById('second2'),
  /** @type {HTMLInputElement} */
  RESULT2: document.getElementById('result2')
}

const calculatePart = () => {
  DOM.RESULT1.value = Calc.calculatePart(DOM.FIRST1.value, DOM.SECOND1.value)
}

const calculatePercent = () => {
  DOM.RESULT2.value = Calc.calculatePercent(DOM.FIRST2.value, DOM.SECOND2.value)
}

document.getElementById('calculateButton1').addEventListener('click', calculatePart);
document.getElementById('calculateButton2').addEventListener('click', calculatePercent);
