/**
 * Class to handle percent calculations
 */
export default class Calc {
    /**
     * Solve percent calculations.
     * @param {number} first number
     * @param {number} second number
     * @returns {number} third number
     */
    static calculatePart(first, second) {
        const percent = second / 100;
        const part = first * percent;
        return part;
    }

    static calculatePercent(first, second) {
        const percent = first*100/second;
        return percent;
    }
}