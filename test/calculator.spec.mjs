import { describe } from 'mocha';
import { expect } from 'chai';
import Calc from '../src/calculator.mjs';

describe('Percent calculator test suite', () => {
    it('Can calculate given percentage from number', () => {
        const first = 200;
        const second = 5;
        const expectedPart = 10;
        expect(Calc.calculatePart(first, second)).to.equal(expectedPart);
    }),
    it('Can calculate the percentage of one number from another', () => {
        const first = 50;
        const second = 200;
        const expectedPercent = 25;
        expect(Calc.calculatePercent(first, second)).to.equal(expectedPercent);
    })
})